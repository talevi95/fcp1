// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDQ52zS8R14AXE_aUoYlyUIbpM3NL0rmDo",
    authDomain: "fcp1-1ba53.firebaseapp.com",
    projectId: "fcp1-1ba53",
    storageBucket: "fcp1-1ba53.appspot.com",
    messagingSenderId: "145070237965",
    appId: "1:145070237965:web:a06dce9b0a031845cf2844",
    measurementId: "G-RZMTJZFEX4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
