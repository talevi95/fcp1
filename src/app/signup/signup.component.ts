import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email: string = "";
  password: string = "";
  isError: boolean = false;
  errorMessage: string = ""; 

  onSubmit(){
    this.auth.signUp(this.email,this.password).then(res => {
      console.log(res);
      this.router.navigate(['/pred-model']); 
    }).catch(
      err => {
        console.log(err);
        this.isError = true; 
        this.errorMessage = err.message; 
      } 
    ) 
  }
  
  constructor(private auth:AuthService, private router:Router) { }
  

  ngOnInit(): void {
  }

}
