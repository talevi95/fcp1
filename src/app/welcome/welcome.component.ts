import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(private router: Router, private activeRoute: ActivatedRoute) { }

  loggedIn: boolean = false;

  ngOnInit(): void {
  }

  onLogin() {
    this.router.navigate(["./login"], {relativeTo: this.activeRoute});
  }

  onSignUp() {
    this.router.navigate(["./signup"], {relativeTo: this.activeRoute});
  }

  onLogout() {
    this.loggedIn = false;
    this.router.navigate(["./login"], {relativeTo: this.activeRoute});
  }

  welcome() {
    this.router.navigate(["./home"], {relativeTo: this.activeRoute});
  }

}
