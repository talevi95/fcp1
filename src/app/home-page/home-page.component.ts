import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  content ="This page will calc based on several details the odds for a heart attack";
  constructor() { }

  ngOnInit(): void {
  }

}
