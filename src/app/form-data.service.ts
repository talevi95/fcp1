import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormDataService {
  url: string = "https://c2xp2kfmg6.execute-api.us-east-1.amazonaws.com/heart";

  public async calcResult(input: any): Promise<any>
  {
    return await this.http.post(this.url, input).toPromise();
  }

  constructor(private http: HttpClient) { }
}
