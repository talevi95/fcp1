export class Answer {
  age: number;
  gender: number;
  pain: number;
  restBp: number;
  cholesterol: number;
  bloodSugar: number;
  restECG: number;
  maxHeartRate: number;
  exang: number;
  oldpeak: number;
  slope: number;
  ca:number;
  thal:number;

  public constructor() 
    {
      this.gender = 0;
      this.pain = 0;
      this.bloodSugar = 0;
      this.exang = 0;
      this.restECG = 0;
      this.slope = 0;
      this.thal = 0;
      this.ca = 0;

    }
}