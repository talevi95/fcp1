import { FormDataService } from './../form-data.service';
import { Component, OnInit } from '@angular/core';
import { Answer } from '../answer.model';
import { NgForm } from '@angular/forms';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'pred-model',
  templateUrl: './pred-model.component.html',
  styleUrls: ['./pred-model.component.css']
})
export class PredModelComponent implements OnInit {

  constructor(private formDataService: FormDataService) { }
  loader: boolean = false;
  resultContent: string; //get back from service
  answer: Answer = new Answer();
  ages: number;
  gender: number[] = [0, 1];
  painLevel: number[] = [0,1,2,3];
  restBp: number;
  cholesterol: number;
  bloodSugar: number[] = [1, 0];
  restECG: number[] = [0,1,2];
  maxHeartRate: number;
  exang: number[] = [1, 0];
  oldpeak: number;
  slope: number[] = [0,1,2];
  ca: number[] = [0,1,2,3,4];
  thal: number[] = [0,1,2,3];		

  ngOnInit(): void {
  }

  async onSubmit(signinForm: NgForm) {
    this.loader = true;
    console.log("XXXX", signinForm)
    let info = [Number(this.answer.age), this.answer.gender, this.answer.pain, Number(this.answer.restBp), 
      Number(this.answer.cholesterol), this.answer.bloodSugar, this.answer.restECG, Number(this.answer.maxHeartRate), 
      this.answer.exang, Number(this.answer.oldpeak), 
      this.answer.slope, this.answer.ca, this.answer.thal];
      if(!signinForm.invalid) {
        let res = await this.formDataService.calcResult({ data: info.toString() });
        if(res == 0) {
          this.resultContent = "NO";
        } else if(res == 1) {
          this.resultContent = "YES";
        }
      } else {
        this.resultContent = null;
      }
      this.loader = false;
  }
}
